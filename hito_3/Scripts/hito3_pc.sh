#!/bin/bash
#Práctica de direccionamento de routers
#Autor: Mario Sánchez García ; NIA: 100315075
#Universidad Carlos III de Madrid, 3º de grado.

#Conexiones necesarias:
#	pc(eth1) - router_X(eth0.4)	
#	
#	router_1(eth0.0) - pc(eth1)
#	router_1(eth0.1) - router_3(eth0.1)
#	router_1(eth0.2) - router_2(eth0.1)
#	router_1(eth0.4) - pc(eth1)
#	
#	router_2(eth0.0) - Libre*
#	router_2(eth0.1) - router_1(eth0.2)
#	router_2(eth0.2) - router_3(eth0.2)
#	router_2(eth0.4) - pc(eth1)
#	
#	router_3(eth0.0) - Libre*
#	router_3(eth0.1) - router_1(eth0.1)
#	router_3(eth0.2) - router_2(eth0.2)
#	router_3(eth0.3) - router_4(eth0.0)
#	router_3(eth0.4) - pc(eth1)
#	
#	router_4(eth0.0) - router_3(eth0.3)
#	router_4(eth0.4) - pc(eth1)

#*Las que son libres se configuran como si se conectasen a una red de hosts, pero no se conectan a nada. 

#Para que pide la contraseña antes de que imprima que se están modificando las IP's y las rutas.
sudo ls > /dev/null &
wait

echo -e "\n------------------------------ HITO 3 ------------------------------\n"

sudo ip addr add 192.168.4.2/24 dev eth1
sudo ip addr add 10.0.75.2/25 dev eth1
sudo ip route add 10.0.75.0/24 10.0.75.1 dev eth1

#--------------------------------- PARA ROUTER 1 ------------------------------------
echo -n "Añadiendo IP's para el router_1..."

#Se añaden las nuevas IPs al router_1 y se eliminan las predefinidas. 
hito3_router1_1.sh | telnet 192.168.4.1 &
#Esperamos a que los cambios se hagan efectivos
wait
sleep 0.1

#------------------------------------------------------------------------------------

echo " Hecho!"

echo "Desconecte la interfaz de eth0.4 del router_1 y conéctelo a eth0.4 del router_2"
sleep 0.1
echo -n "(Pulsa \"S\" para confirmar que ha conectado la interfaz)... "
# Leemos la entrada del usuario
read -n 1 -s
echo ""


#--------------------------------- PARA ROUTER 2 ------------------------------------
echo -n "Añadiendo IP's para el router_2..."

#Se añaden las nuevas IPs al router_2 y se eliminan las predefinidas. 
hito3_router2_1.sh | telnet 192.168.4.1 &
#Esperamos a que se hagan efectivos los cambios
wait
sleep 0.1

#------------------------------------------------------------------------------------

echo " Hecho!"

echo "Desconecte la interfaz de eth0.4 del router_2 y conéctelo a eth0.4 del router_3"
sleep 0.1
echo -n "(Pulsa \"S\" para confirmar que ha conectado la interfaz)... "
# Leemos la entrada del usuario
read -n 1 -s
echo ""

#--------------------------------- PARA ROUTER 3 ------------------------------------
echo -n "Añadiendo IP's para el router_3..."

#Se añaden las nuevas IPs al router_3 y se eliminan las predefinidas. 
hito3_router3_1.sh | telnet 192.168.4.1 &
#Esperamos a que se hagan efectivos los cambios
wait
sleep 0.1

#------------------------------------------------------------------------------------

echo " Hecho!"

echo "Desconecte la interfaz de eth0.4 del router_3 y conéctelo a eth0.4 del router_4"
sleep 0.1
echo -n "(Pulsa \"S\" para confirmar que ha conectado la interfaz)... "
# Leemos la entrada del usuario
read -n 1 -s
echo ""

#--------------------------------- PARA ROUTER 4 ------------------------------------
echo -n "Añadiendo IP's para el router_4..."

#Se añaden las nuevas IPs al router_4 y se eliminan las predefinidas. 
hito3_router4_1.sh | telnet 192.168.4.1 &
#Esperamos a que se hagan efectivos los cambios
wait
sleep 0.1

#------------------------------------------------------------------------------------

echo " Hecho!"

echo "Desconecte la interfaz de eth0.4 del router_4 y conéctelo a eth0.0 del router_1"
sleep 0.1
echo -n "(Pulsa \"S\" para confirmar que ha conectado la interfaz)... "
# Leemos la entrada del usuario
read -n 1 -s
echo ""


echo -e "Ping a 10.0.75.177(pata router_4) desde host_1:\n"
ping -w 5 10.0.75.177

echo -e "\nTraceroute a 10.0.75.177(pata router_4) desde host_1:\n"
traceroute 10.0.75.177


#------------------------------------------------------------------------------------
echo -n -e "\nActivando protocolo RIP..."

#Se conecta con la nueva IP al router_1 para activar RIP
hito3_router1_2.sh | telnet 10.0.75.1 &
#Esperamos a que los cambios se hagan efectivos
wait
sleep 0.1

#Se conecta con la nueva IP al router_2 para activar RIP
hito3_router2_2.sh | telnet 10.0.75.189 &
#Esperamos a que los cambios se hagan efectivos
wait
sleep 0.1

#Se conecta con la nueva IP al router_3 para activar RIP
hito3_router3_2.sh | telnet 10.0.75.185 &
#Esperamos a que los cambios se hagan efectivos
wait
sleep 0.1

#Se conecta con la nueva IP al router_4 para activar RIP
hito3_router4_2.sh | telnet 10.0.75.177 &
#Esperamos a que los cambios se hagan efectivos
wait
sleep 0.1

echo " Hecho!"
echo -e "Traceroute a 10.0.75.177(pata router_4) desde host_1:\n"
traceroute 10.0.75.177

echo -e "\n--------------------------------------------------------------------\n"