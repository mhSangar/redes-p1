### ¿Para qué sirve este repositorio? ###

Este repositorio permite alcanzar los hitos de la práctica de enrutamiento de routers de forma automática mediante el uso de *Shell Script*.

### ¿Cómo lo pongo en marcha? ###

1. Disponer de 4 routers Linksys WRT54GS/GL y un PC con una tarjeta de red.
2. Realizar las conexiones mediante interfaces RJ-45 tal y como explica el archivo "Conexiones entre dispositivos".
3. Iniciar sesión en el PC con Debian/Ubuntu.
4. Ejecutar uno de los archivos "exec_hito..." que se encuentran en la raíz del repositorio, que se identifican con el hito que permiten alcanzar; y seguir los pasos que se indican en la terminal.

* En caso de querer ejecutar otro de los hitos, antes se deben reiniciar los routers.

### ¿Quién eres? ###

* Dueño y autor: Mario Sánchez García
* Institución: Universidad Carlos III de Madrid