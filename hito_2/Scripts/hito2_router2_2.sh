#!/bin/bash
#Práctica de direccionamento de routers
#Autor: Mario Sánchez García ; NIA: 100315075
#Universidad Carlos III de Madrid, 3º de grado.

#Terminal de router_2
echo "configure terminal"
sleep 0.1
	echo "ip route 10.0.75.0/25 10.0.75.190"
	echo "ip route 10.0.75.0/25 10.0.75.181 2"
	echo "ip route 10.0.75.160/28 10.0.75.181"
	echo "ip route 10.0.75.160/28 10.0.75.190 2"
	echo "ip route 0.0.0.0/0 10.0.75.181"
	echo "ip route 0.0.0.0/0 10.0.75.190 2"
	sleep 0.1
	echo "exit"
echo "exit"
#Salimos del router_2