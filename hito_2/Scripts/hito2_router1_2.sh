#!/bin/bash
#Práctica de direccionamento de routers
#Autor: Mario Sánchez García ; NIA: 100315075
#Universidad Carlos III de Madrid, 3º de grado.

#Terminal del router_1
echo "configure terminal"
sleep 0.1
	echo "ip route 10.0.75.128/27 10.0.75.189"
	echo "ip route 10.0.75.128/27 10.0.75.185 2"
	echo "ip route 10.0.75.160/28 10.0.75.185"
	echo "ip route 10.0.75.160/28 10.0.75.189 2"
	echo "ip route 0.0.0.0/0 10.0.75.185"
	echo "ip route 0.0.0.0/0 10.0.75.189 2"
	sleep 0.1
	echo "exit"
echo "exit"
#Salimos del router_1

