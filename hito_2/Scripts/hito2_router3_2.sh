#!/bin/bash
#Práctica de direccionamento de routers
#Autor: Mario Sánchez García ; NIA: 100315075
#Universidad Carlos III de Madrid, 3º de grado.

#Terminal del router_3
echo "configure terminal"
sleep 0.1
	echo "ip route 10.0.75.0/25 10.0.75.186"
	echo "ip route 10.0.75.0/25 10.0.75.182 2"
	echo "ip route 10.0.75.128/27 10.0.75.182"
	echo "ip route 10.0.75.128/27 10.0.75.186 2"
	echo "ip route 0.0.0.0/0 10.0.75.177"
	sleep 0.1
	echo "exit"
echo "exit"
#Salimos del router_3