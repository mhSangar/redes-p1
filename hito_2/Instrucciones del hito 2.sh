#Práctica de direccionamento de routers
#Autor: Mario Sánchez García ; NIA: 100315075
#Universidad Carlos III de Madrid, 3º de grado.

#Conexiones necesarias:
#	pc(eth1) - router_1(eth0.0)	
#	
#	router_1(eth0.0) - pc(eth1)
#	router_1(eth0.1) - router_3(eth0.1)
#	router_1(eth0.2) - router_2(eth0.1)
#	
#	router_2(eth0.0) - Libre*
#	router_2(eth0.1) - router_1(eth0.2)
#	router_2(eth0.2) - router_3(eth0.2)
#	
#	router_3(eth0.0) - Libre*
#	router_3(eth0.1) - router_1(eth0.1)
#	router_3(eth0.2) - router_2(eth0.2)
#	router_3(eth0.3) - router_4(eth0.0)
#	
#	router_4(eth0.0) - router_3(eth0.3)

#*Las que son libres se configuran como si se conectasen a una red de hosts, pero no se conectan a nada. 

#Para que pida la contraseña antes de que imprima que se están modificando las IP's y las rutas.
sudo ls > /dev/null &
wait

echo -n "Añadiendo IP's y rutas estáticas..."

sudo ip addr add 192.168.0.2/24 dev eth1
sudo ip addr add 10.0.75.2/25 dev eth1
sudo ip route add 10.0.75.0/24 10.0.75.1 dev eth1
telnet 192.168.0.1
#Terminal del router_1
	configure terminal
		interface eth0.0
			ip address 10.0.75.1/25
			exit
		interface eth0.1
			ip address 10.0.75.186/30
			no ip address 192.168.1.1/24
			#Para conectarse al router_3(eth0.1)
			ip address 192.168.1.100/24
			exit
		interface eth0.2
			ip address 10.0.75.190/30
			no ip address 192.168.2.1/24
			#Para conectarse al router_2(eth0.1)
			ip address 192.168.1.101/24
			exit
		exit
	exit
#Terminal del PC
telnet 10.0.75.1
	#Terminal del router_1
	configure terminal
		interface eth0.0
			no ip address 192.168.0.1/24
			exit
		ip route 10.0.75.128/27 10.0.75.189
		ip route 10.0.75.128/27 10.0.75.185 2
		ip route 10.0.75.160/28 10.0.75.185
		ip route 10.0.75.160/28 10.0.75.189 2
		ip route 0.0.0.0/0 10.0.75.185
		ip route 0.0.0.0/0 10.0.75.189 2
		exit
	telnet 192.168.1.1
	#Terminal router_2
	configure terminal
		interface eth0.0
			ip address 10.0.75.129/27
			no ip address 192.168.0.1/24
			exit
		interface eth0.1
			ip address 10.0.75.189/30
			exit
		interface eth0.2
			ip address 10.0.75.182/30
			no ip address 192.168.2.1/24
			exit
		exit
	exit
	#Terminal del router_1
	telnet 10.0.75.189
	#Terminal del router_2
	configure terminal
		interface eth0.1
			no ip address 192.168.1.1/24
			exit
		ip route 10.0.75.0/25 10.0.75.190
		ip route 10.0.75.0/25 10.0.75.181 2
		ip route 10.0.75.160/28 10.0.75.181
		ip route 10.0.75.160/28 10.0.75.190 2
		ip route 0.0.0.0/0 10.0.75.181
		ip route 0.0.0.0/0 10.0.75.190 2
		exit
	exit
	#Terminal del router_1
	telnet 192.168.2.1
	#Terminal del router_3
	configure terminal
		interface eth0.0
			ip address 10.0.75.161/28
			no ip address 192.168.0.1/24
			exit
		interface eth0.1
			ip address 10.0.75.181/30
			no ip address 192.168.1.1/30
			exit
		interface eth0.2
			ip address 10.0.75.185/30
			exit
		interface eth0.3
			ip address 10.0.75.178/30
			no ip address 192.168.3.1/24
			#Para conectarse con router_4
			ip address 192.168.0.100/24
			exit
		exit
	exit
	#Terminal del router_1
	telnet 10.0.75.185
	#Terminal del router_3
	configure terminal
		interface eth0.2
			no ip address 192.168.2.1/24
			exit
		ip route 10.0.75.0/25 10.0.75.186
		ip route 10.0.75.0/25 10.0.75.182 2
		ip route 10.0.75.128/27 10.0.75.182
		ip route 10.0.75.128/27 10.0.75.186 2
		ip route 0.0.0.0/0 10.0.75.177
		exit
	telnet 192.168.0.1
	#Terminal del router_4
	configure terminal
		interface eth0.0
			ip address 10.0.75.177/30
			exit
		exit
	exit
	#Terminal del router_3
	telnet 10.0.75.177
	#Terminal del router_4
	configure terminal
		interface eth0.0
			no ip address 192.168.0.1/24
			exit
		ip route 10.0.75.0/24 10.0.75.178
		exit
	exit
	#Terminal del router_3
	configure terminal
		interface eth0.3
			no ip address 192.168.3.100/24
			exit
		exit
	exit
	#Terminal del router_1
	configure terminal
		interface eth0.1
			no ip address 192.168.1.100/24
			exit
		interface eth0.2
			no ip address 192.168.1.101/24
			exit
		exit
	exit
#Terminal de PC
echo " Hecho!"
echo -e "Ping a 10.0.75.177(pata router_4) desde host_1:\n"
ping -w 5 10.0.75.177
echo -e "\nTraceroute a 10.0.75.177(pata router_4) desde host_1:\n"
traceroute 10.0.75.177

echo -n -e "\nRompiendo enlace entre router_1 y router_3..."
telnet 10.0.75.1
#Terminal de router_1
	configure terminal
		interface eth0.1
			shutdown
			exit
		exit
	telnet 10.0.75.185
	#Terminal de router_3
	configure terminal
		interface eth0.1
			shutdown
			exit
		exit
	exit
	#Terminal de router_1
	exit
#Terminal PC
echo -e " Hecho!"

echo -e "Traceroute a 10.0.75.177(pata router_4) desde host_1:\n"
traceroute 10.0.75.177





