#!/bin/bash
#Práctica de direccionamento de routers
#Autor: Mario Sánchez García ; NIA: 100315075
#Universidad Carlos III de Madrid, 3º de grado.

#Terminal del router_1
echo "configure terminal"
sleep 0.1
	echo "interface eth0.0"
	sleep 0.1
		echo "ip address 10.0.75.1/25"
		echo "no ip address 192.168.0.1/24"
		echo "exit"
		sleep 0.1
	echo "interface eth0.1"
	sleep 0.1
		echo "ip address 10.0.75.186/30"
		echo "no ip address 192.168.1.1/24"
		echo "exit"
		sleep 0.1
	echo "interface eth0.2"
	sleep 0.1
		echo "ip address 10.0.75.190/30"
		echo "no ip address 192.168.2.1/24"
		sleep 0.1
		echo "exit"
	echo "exit"
echo "exit"
#Salimos del router_1