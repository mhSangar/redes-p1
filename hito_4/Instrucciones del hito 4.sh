#Práctica de direccionamento de routers
#Autor: Mario Sánchez García ; NIA: 100315075
#Universidad Carlos III de Madrid, 3º de grado.

#Conexiones necesarias:
#	pc(eth1) - router_1(eth0.0)	
#	
#	router_1(eth0.0) - pc(eth1)
#	router_1(eth0.1) - router_3(eth0.1)
#	router_1(eth0.2) - router_2(eth0.1)
#	
#	router_2(eth0.0) - Libre*
#	router_2(eth0.1) - router_1(eth0.2)
#	router_2(eth0.2) - router_3(eth0.2)
#	
#	router_3(eth0.0) - Libre*
#	router_3(eth0.1) - router_1(eth0.1)
#	router_3(eth0.2) - router_2(eth0.2)
#	router_3(eth0.3) - router_4(eth0.0)
#	
#	router_4(eth0.0) - router_3(eth0.3)

#*Las que son libres se configuran como si se conectasen a una red de hosts, pero no se conectan a nada. 

# Para que pida la contraseña antes de que se se imprime el mensaje que de la modificación de las IP's.
sudo ls > /dev/null	&
wait

echo -n "Añadiendo IP's..."

sudo ip addr add 192.168.0.2/24 dev eth1
sudo ip addr add 10.0.75.2/24 dev eth1
sudo ip route add 10.0.75.0/24 10.0.75.1 dev eth1
telnet 192.168.0.1
#Terminal del router_1
	configure terminal
		interface eth0.0
			ip address 10.0.75.1/25
			exit
		interface eth0.1
			ip address 10.0.75.186/30
			no ip address 192.168.1.1/24
			#Para conectarse al router_3(eth0.1)
			ip address 192.168.1.100/24
			exit
		interface eth0.2
			ip address 10.0.75.190/30
			no ip address 192.168.2.1/24
			#Para conectarse al router_2(eth0.1)
			ip address 192.168.1.101/24
			exit
		exit
	exit
#Terminal del PC
telnet 10.0.75.1
#Terminal del router_1
	configure terminal
		interface eth0.0
			no ip address 192.168.0.1/24
			exit
		exit
	telnet 192.168.1.1
	#Terminal router_2
	configure terminal
		interface eth0.0
			ip address 10.0.75.129/27
			no ip address 192.168.0.1/24
			exit
		interface eth0.1
			ip address 10.0.75.189/30
			exit
		interface eth0.2
			ip address 10.0.75.182/30
			no ip address 192.168.2.1/24
			exit
		exit
	exit
	#Terminal del router_1
	telnet 10.0.75.189
	#Terminal del router_2
	configure terminal
		interface eth0.1
			no ip address 192.168.1.1/24
			exit
		exit
	exit
	#Terminal del router_1
	telnet 192.168.2.1
	#Terminal del router_3
	configure terminal
		interface eth0.0
			ip address 10.0.75.161/28
			no ip address 192.168.0.1/24
			exit
		interface eth0.1
			ip address 10.0.75.181/30
			no ip address 192.168.1.1/30
			exit
		interface eth0.2
			ip address 10.0.75.185/30
			exit
		interface eth0.3
			ip address 10.0.75.178/30
			no ip address 192.168.3.1/24
			#Para conectarse con router_4
			ip address 192.168.0.100/24
			exit
		exit
	exit
	#Terminal del router_1
	telnet 10.0.75.185
	#Terminal del router_3
	configure terminal
		interface eth0.2
			no ip address 192.168.2.1/24
			exit
		exit
	telnet 192.168.0.1
	#Terminal del router_4
	configure terminal
		interface eth0.0
			ip address 10.0.75.177/30
			exit
		exit
	exit
	#Terminal del router_3
	telnet 10.0.75.177
	#Terminal del router_4
	configure terminal
		interface eth0.0
			no ip address 192.168.0.1/24
			exit
		exit
	exit
	#Terminal del router_3
	configure terminal
		interface eth0.3
			no ip address 192.168.3.100/24
			exit
		exit
	exit
	#Terminal del router_1
	configure terminal
		interface eth0.1
			no ip address 192.168.1.100/24
			exit
		interface eth0.2
			no ip address 192.168.1.101/24
			exit
		exit
	exit
#Terminal de PC
echo " Hecho!"

echo -n "Activando protocolo RIP..."
telnet 10.0.75.1
#Terminal de router_1
	configure terminal
		router rip
			network eth0.0
			network eth0.1
			network eth0.2
			exit
		exit
	telnet 10.0.75.189
	#Terminal de router_2
	configure terminal
		router rip
			network eth0.0
			network eth0.1
			network eth0.2
			exit
		exit
	exit
	#Terminal de router_1
	telnet 10.0.75.185
	#Terminal de router_3
	configure terminal
		router rip
			network eth0.0
			network eth0.1
			network eth0.2
			network eth0.3
			exit
		exit
	telnet 10.0.75.177
	#Terminal de router_4
	configure terminal
		router rip
			network eth0.0
			exit
		exit
	exit
	#Terminal de router_3
	exit
	#Terminal de router_1
	exit
#Terminal PC
echo -e " Hecho!\n"

echo -e "\nCortar conexión entre router_1 y router_2 durante el ping\n"
ping 10.0.75.177
